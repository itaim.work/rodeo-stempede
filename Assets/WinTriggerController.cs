using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinTriggerController : MonoBehaviour
{
    [SerializeField] private UI_Manager UI_Manager_Script;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            UI_Manager_Script.WinGame();
        }
    }

}
