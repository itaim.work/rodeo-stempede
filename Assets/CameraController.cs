using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{

    [SerializeField] private Transform Player;
    [SerializeField] private Vector3 NewPosition;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        NewPosition = new Vector3(transform.position.x, transform.position.y, Player.position.z - 15);
        transform.position = NewPosition;
    }
}
