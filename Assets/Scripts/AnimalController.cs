using UnityEngine;
using UnityEngine.AI;
using System;


public class AnimalController : MonoBehaviour
{
    public enum State
    {
        AnimalControll,
        SimpleAnimalControll,
        playerController
    }

    [SerializeField] private NavMeshAgent AnimaleAgent;

    [SerializeField] private GameObject[] Targets;

    [SerializeField] private TargetManager TargetManagerCode;

    Animator anim;
    private int counter = 0;

    [SerializeField] private bool Stop = false, isSimple = false, foundTargetSimple;

    [SerializeField] private CharacterController Controller;

    [SerializeField] private float PlayerSpeed, forwardSpeed, VerticalInput, Timer, TimerReset;

    [SerializeField] private State ActiveState;



    private float HorizontalInput;
    private Vector3 moveDirection, SimpleTarget;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        Timer = TimerReset;
        AnimaleAgent = gameObject.GetComponent<NavMeshAgent>();
        Controller = gameObject.GetComponent<CharacterController>();

        char animel_ID = gameObject.name[gameObject.name.Length - 1];

        TargetManagerCode = GameObject.Find("TargetManager").GetComponent<TargetManager>();

        Targets = TargetManagerCode.getTargetArray();

        if (ActiveState == State.SimpleAnimalControll)
        {
            SimpleTarget = new Vector3(UnityEngine.Random.Range(-12, 14), 0, transform.position.z + 30);
        }
        anim.SetInteger("Walk", 1);

        if (ActiveState == State.AnimalControll)
        {
            AnimaleAgent.SetDestination(Targets[counter++].transform.position);
        }

    }

    // Update is called once per frame
    void Update()
    {

        switch (ActiveState)
        {
            case State.AnimalControll:
                Timer = TimerReset;
                if (AnimaleAgent.remainingDistance < 0.5f && !Stop)
                {
                    AnimaleAgent.SetDestination(Targets[counter++].transform.position);
                }
                if (counter == Targets.Length)
                {
                    Stop = true;
                }
                break;
            case State.SimpleAnimalControll:
                Timer = TimerReset;
                if (AnimaleAgent.remainingDistance < 0.5f && foundTargetSimple)
                {
                    Destroy(gameObject);
                }
                if (!foundTargetSimple)
                {
                    AnimaleAgent.SetDestination(SimpleTarget);
                }
                foundTargetSimple = true;
                break;
            case State.playerController:
                HorizontalInput = Input.GetAxis("Horizontal");
                VerticalInput = Input.GetAxis("Vertical");
                moveDirection = new Vector3(HorizontalInput, 0f, forwardSpeed * VerticalInput + 2);
                moveDirection = moveDirection * Time.deltaTime * PlayerSpeed;
                Controller.Move(moveDirection);

                break;
        }

        if (Timer <= 0)
        {
            print("bad");
        }
        Timer -= Time.deltaTime;
    }

    public int CompareByName(GameObject a, GameObject b)
    {
        return -a.transform.name.CompareTo(b.transform.name);
    }

    public void SwitchControll()
    {
        if (ActiveState == State.playerController)
        {
            if (isSimple)
            {
                ActiveState = State.SimpleAnimalControll;
                AnimaleAgent.enabled = true;
                Controller.enabled = false;

            }
            else
            {
                ActiveState = State.AnimalControll;
                AnimaleAgent.enabled = true;
                Controller.enabled = false;

            }
        }
        else
        {
            ActiveState = State.playerController;
            AnimaleAgent.enabled = false;
            Controller.enabled = true;
        }

    }

}
