using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFollow : MonoBehaviour
{
    [SerializeField] private Transform ActiveAnimle;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(ActiveAnimle.position.x, transform.position.y, ActiveAnimle.position.z);
    }

    public void SetActiveAnimle(Transform NewActiveAnimle)
    {
        ActiveAnimle = NewActiveAnimle;
    }
    public GameObject GetActiveAnimal()
    {
        return ActiveAnimle.gameObject;
    }
}
